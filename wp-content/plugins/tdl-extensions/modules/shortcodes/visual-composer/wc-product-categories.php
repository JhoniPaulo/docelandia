<?php

// [product_categories]

vc_map(array(
   "name"			=> esc_html__('Product Categories - Thumbs', 'eva'),
   "category"		=> 'WooCommerce',
   "description"	=> "",
   "base"			=> "product_categories",
   "class"			=> "",
   "icon"			=> "product_categories",
   
   "params" 	=> array(
  	
      
		array(
			"type"			=> "textfield",
			"holder"		=> "div",
			"class" 		=> "hide_in_vc_editor",
			"admin_label" 	=> true,
			"heading"		=> esc_html__('How many product categories to display?', 'eva'),
			"param_name"	=> "number",
			"value"			=> "",
		),
		
		array(
			"type"			=> "textfield",
			"holder"		=> "div",
			"class" 		=> "hide_in_vc_editor",
			"admin_label" 	=> true,
			"heading"		=> esc_html__('Columns', 'eva'),
			"param_name"	=> "columns",
			"value"			=> "",
		),
		
		array(
			"type"			=> "dropdown",
			"holder"		=> "div",
			"class" 		=> "hide_in_vc_editor",
			"admin_label" 	=> true,
			"heading"		=> "Order By",
			"param_name"	=> "orderby",
			"value"			=> array(
				__( 'Date', 'js_composer' ) => 'date',
				__( 'ID', 'js_composer' ) => 'ID',
				__( 'Author', 'js_composer' ) => 'author',
				__( 'Title', 'js_composer' ) => 'title',
				__( 'Modified', 'js_composer' ) => 'modified',
				__( 'Random', 'js_composer' ) => 'rand',
				__( 'Comment count', 'js_composer' ) => 'comment_count',
				__( 'Menu order', 'js_composer' ) => 'menu_order',
			),
		),
		
		array(
			"type"			=> "dropdown",
			"holder"		=> "div",
			"class" 		=> "hide_in_vc_editor",
			"admin_label" 	=> true,
			"heading"		=> "Order",
			"param_name"	=> "order",
			"value"			=> array(
				__( 'Descending', 'js_composer' ) => 'DESC',
				__( 'Ascending', 'js_composer' ) => 'ASC',
			),
		),
		
		array(
			"type"			=> "dropdown",
			"holder"		=> "div",
			"class" 		=> "hide_in_vc_editor",
			"admin_label" 	=> true,
			"heading"		=> esc_html__('Hide Empty', 'eva'),
			"param_name"	=> "hide_empty",
			"value"			=> array(
				"Yes"	=> "1",
				"No"	=> "0"
			),
		),
		
		array(
			"type"			=> "textfield",
			"holder"		=> "div",
			"class" 		=> "hide_in_vc_editor",
			"admin_label" 	=> true,
			"heading"		=> esc_html__('Parent', 'eva'),
			"description"	=> esc_html__('Set the parent paramater to 0 to only display top level categories.', 'eva'),
			"param_name"	=> "parent",
			"value"			=> "",
		),
		
		// array(
		// 	"type"			=> "textfield",
		// 	"holder"		=> "div",
		// 	"class" 		=> "hide_in_vc_editor",
		// 	"admin_label" 	=> true,
		// 	"heading"		=> esc_html__('IDs', 'eva'),
		// 	"description"	=> esc_html__('Set ids to a comma separated list of category ids to only show those.', 'eva'),
		// 	"param_name"	=> "ids",
		// 	"value"			=> "",
		// ),

		array(
			'type' => 'autocomplete',
			'heading' => __( 'Categories', 'js_composer' ),
			'param_name' => 'ids',
			'settings' => array(
				'multiple' => true,
				'sortable' => true,
							),
			'save_always' => true,
			'description' => __( 'List of product categories', 'js_composer' ),
		),		
   )
   
));