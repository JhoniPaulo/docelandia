<?php
if (!defined('ABSPATH')) {
    die;
}
?>
<script>
var focused=0;
guaven_data_path='<?php echo  plugin_dir_url(__FILE__).'assets/guaven_woos_data.js?v='.$this->js_css_version;?>';
guaven_woos_idb_version=<?php echo (int)$this->js_css_version;?>;
guaven_engine_start_delay=<?php echo wp_is_mobile()?700:500;  ?>;
guaven_woos_disable_focusout=0;
guaven_show_all_text="<?php echo $this->get_string(get_option('guaven_show_all_text'));?>";
guaven_woos_showinit="<?php echo get_option('guaven_woos_showinit') !='' ? $this->get_string(get_option('guaven_woos_showinit_t'),'html') : '';?>";
guaven_woos_shownotfound="<?php echo $this->get_string(get_option('guaven_woos_showinit_n'),'html');?>";
guaven_woos_populars_enabled=<?php   echo get_option('guaven_woos_nomatch_pops') != '' ? 1 : 0; ?>;
guaven_woos_categories_enabled=<?php echo get_option('guaven_woos_catsearch') != '' ? 1 : 0;?>;
cmaxcount=<?php echo get_option('guaven_woos_catsearchmax') > 0 ? (int)get_option('guaven_woos_catsearchmax') : 5;?>;
guaven_woos_correction_enabled=<?php  echo get_option('guaven_woos_corr_act') != '' ? 1 : 0;?>;
guaven_woos_pinnedtitle="<?php if (get_option('guaven_woos_ispin') != '') echo $this->get_string(get_option('guaven_woos_pinnedt'),'html');?>";
guaven_woos_sugbarwidth=<?php echo get_option('guaven_woos_sugbarwidth') > 0 ? (round(get_option('guaven_woos_sugbarwidth') * 100) / 10000) : '1';?>;
minkeycount=<?php echo get_option('guaven_woos_min_symb_sugg') > 0 ? (int)get_option('guaven_woos_min_symb_sugg') : 3;?>;
maxcount=<?php echo get_option('guaven_woos_maxres') > 0 ? (int)get_option('guaven_woos_maxres') : 10;?>;
maxtypocount=<?php echo get_option('guaven_woos_whentypo') > 0 ? (int)get_option('guaven_woos_whentypo') : 'maxcount';?>;
guaven_woos_large_data=<?php echo get_option('guaven_woos_large_data') > 0 ? (int)get_option('guaven_woos_large_data') : '0';?>;
guaven_woos_updir="<?php  $wpupdir=wp_upload_dir();echo $wpupdir['baseurl'];?>";
guaven_woos_exactmatch=<?php echo get_option('guaven_woos_exactmatch') > 0 ? (int)get_option('guaven_woos_exactmatch') : '0';?>;
guaven_woos_backend=<?php echo get_option('guaven_woos_backend') !='' ? 1 : 0;?>;
guaven_woos_perst="<?php if ($this->cookieprods != '' and !empty($_COOKIE["guaven_woos_lastvisited"])) echo $this->get_string(get_option('guaven_woos_perst'));?>";
guaven_woos_persprod="<?php if ($this->cookieprods != '' and !empty($_COOKIE["guaven_woos_lastvisited"])) echo  $this->get_string(stripslashes($_COOKIE["guaven_woos_lastvisited"]),'html');?>";
guaven_woos_mobilesearch=<?php echo get_option('guaven_woos_mobilesearch')!=''?1:0;?>;
guaven_woos_ignorelist=[<?php $ignorearr=explode(",",get_option('guaven_woos_ignorelist')); foreach($ignorearr as $iarr) echo '"'.addslashes($iarr).'",'; ?>];
<?php
if (get_option('guaven_woos_data_tracking')!=''){
  ?>
guaven_woos_dttrr=1;
if (typeof(Storage) !== "undefined") {
guaven_woos_data = {
  "action": "guaven_woos_tracker",
  "ajnonce": "<?php  $controltime=time();  echo wp_create_nonce('guaven_woos_tracker_'.$controltime);?>",
  "addcontrol": "<?php echo $controltime; ?>",
};
guaven_woos_ajaxurl="<?php echo admin_url( 'admin-ajax.php' );?>";
}
  <?php
}
else echo 'guaven_woos_dttrr=0;';
        if (get_option('guaven_woos_nojsfile') != '') {
            echo get_option('guaven_woos_js_data');
        }
?>
guaven_woos_wpml="<?php echo (defined('ICL_LANGUAGE_CODE')?'woolan_'.ICL_LANGUAGE_CODE:'');?>";
guaven_woos_homeurl="<?php $guaven_woos_pure_home= explode("?",home_url()); echo $guaven_woos_pure_home[0]; ?>";
  </script>
  <style>
<?php  if (get_option('guaven_woos_custom_css') != '') {
            echo stripslashes(get_option('guaven_woos_custom_css'));
        }
        ?>
  </style>
<?php if (get_option('guaven_woos_mobilesearch')!=''){ ?>
  <div class="guaven_woos_mobilesearch">
  <p><a onclick='guaven_woos_mobclose()' href="javascript://">X</a></p>
  <form action="" method="get"> <input type="hidden" name="post_type" value="product">
  <input name="s" id="guaven_woos_s" type="text"></form>
  </div>
  <?php } ?>
  <div class="guaven_woos_suggestion"> </div>
