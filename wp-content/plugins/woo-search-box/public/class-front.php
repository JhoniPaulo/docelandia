<?php

if (!defined('ABSPATH')) {
    die;
}

class Guaven_woo_search_front
{
    private $cookieprods;
    public $js_css_version;

    public function __construct()
    {
        if (get_option('guaven_woos_ispers') != '') {
            $this->cookieprods = 1;
        }
        $this->js_css_version = (int) get_option('guaven_woos_jscss_version') + 1.54;
    }

    public function personal_interest_collector()
    {
        if (is_singular('product') and !empty($this->cookieprods)) {
            global $post;

            $products_personal = '';

            if (!empty($_COOKIE['guaven_woos_lastvisited'])) {
                $products_personal = stripslashes($_COOKIE['guaven_woos_lastvisited']);
            }

            $guaven_woo_search_admin = new Guaven_woo_search_admin();

            $_regular_price = get_post_meta($post->ID, '_regular_price', true);
            if ((int) $_regular_price == 0) {
                $_regular_price = get_post_meta($post->ID, '_price', true);
            }

            $sale_expiration = (int) strtotime(get_post_meta($post->ID, '_sale_price_dates_to', true)) - time();
            $sale_start = (int) strtotime(get_post_meta($post->ID, '_sale_price_dates_from', true)) - time();
            $sale_meta = ($sale_start < 0 and $sale_expiration > 0) ? get_post_meta($post->ID, '_sale_price', true) : '';

            if ($_regular_price > 0) {
                $_regular_price = number_format((float) $_regular_price, 2, '.', ' ');
            }
            if ($sale_meta > 0) {
                $sale_meta = number_format((float) $sale_meta, 2, '.', ' ');
            }

            $new_products_personal = array(
                'ID' => $post->ID,
                'thumb' => wp_get_attachment_thumb_url(get_post_thumbnail_id($post->ID)),
                'url' => get_permalink($post->ID),
                'title' => get_the_title($post->ID),
                'price' => $_regular_price,
                'sale' => $sale_meta,
                'currency' => get_woocommerce_currency_symbol(),
            );

            $htmlkeys = '<li class=\"guaven_woos_suggestion_list\" tabindex=\"'.$post->ID.'\">  '.$guaven_woo_search_admin->parse_template($new_products_personal, 'persprod').' </li>';

            $products_personal = str_replace($htmlkeys, '', $products_personal);

            $max_cookie_res_count = get_option('guaven_woos_persmax') > 0 ? get_option('guaven_woos_persmax') : 5;

            $htmlkeys .= $products_personal;

            $htmlkeys_arr = explode('<li', $htmlkeys);
            if (count($htmlkeys_arr) > ($max_cookie_res_count + 1)) {
                $htmlkeys_arr = array_slice($htmlkeys_arr, 0, ($max_cookie_res_count + 1));
                $htmlkeys = implode('<li', $htmlkeys_arr);
            }

            setcookie('guaven_woos_lastvisited', $htmlkeys, time() + 86400, '/', null, 0);
        }
    }

    public function enqueue()
    {
        if (get_option('guaven_woos_firstrun') != '') {
          $enqname='guaven_woos';
            wp_enqueue_script('guaven_woos', plugin_dir_url(__FILE__).'assets/'.$enqname.'.js', array(
                'jquery',
            ), $this->js_css_version, true);
        }

        wp_enqueue_style('guaven_woos', plugin_dir_url(__FILE__).'assets/guaven_woos.css', array(), $this->js_css_version);
    }

    public function inline_js()
    {
        require_once plugin_dir_path(dirname(__FILE__)).'public/view.php';
    }

    public function kses($str)
    {
        return addslashes(wp_kses(stripslashes($str),
      array('a'=>array('href' => array(),'class'=>array()),'img'=>array('src' => array(),'class'=>array()),'p'=>array('class'=>array()),
      'strong'=>array(),'i'=>array(),'em'=>array(),'ul'=>array(),'ol'=>array(),'li'=>array(),'b'=>array(),'br'=>array(),'div'=>array('class'=>array()),'span'=>array('class'=>array()),'small'=>array())));
    }

    public function get_string($str, $stype='')
    {
        if (strpos($str, 'wpml') === false) {
            return $stype=='html'?$this->kses($str):esc_js($str);
        }

        if (defined('ICL_LANGUAGE_CODE')) {
            $filtered=urldecode(html_entity_decode(esc_attr($str)));
            $strarr = simplexml_load_string($filtered);
            $current_language = ICL_LANGUAGE_CODE;
            if (!empty($strarr->$current_language)) {
                return $stype=='html'?$this->kses($strarr->$current_language):esc_js($strarr->$current_language);
            }
        }
        return $stype=='html'?$this->kses($str):esc_js($str);
    }

public function query_checker($where,$keyword){
  $keyword_arr=explode(" ",$keyword);
  foreach ($keyword_arr as $kwkey => $kwvalue) {
  $where=str_replace(array(']'.$kwvalue.'[','%'.$kwvalue.'%'),array('][','%%'),$where);
  }
  $where=str_replace("AND post_title REGEXP '[[:<:]][[:>:]]'","",$where);
  return $where;
}

    public function backend_search_filter($where = '')
{
    $is_woo = 1;
    $where2=$where;
    $checkkeyword='';$sanitize_cookie='';
    $backend_enable=get_option('guaven_woos_backend');
    if (!empty($_COOKIE['prids_object_cookie']) ) {
        $sanitize_cookie=preg_replace("/[^0-9,.]/", "", $_COOKIE['prids_object_cookie']);
    }
    if (!empty($_COOKIE['prids_keyword_cookie'])) {
     $checkkeyword=urldecode($_COOKIE['prids_keyword_cookie']);
 }

    if ($sanitize_cookie!='') $sanitize_cookie_final=esc_sql(substr($sanitize_cookie, 0, -1)); else $sanitize_cookie_final='';

    if ($backend_enable!='' and !is_admin() and isset($_GET['s']) and $checkkeyword==$_GET['s']
     and isset($_GET['post_type']) and $_GET['post_type'] == 'product' and !empty($checkkeyword)
    //  and strpos($where,$checkkeyword)!==false
      and is_main_query()
    ) {
        global $wpdb;
        if (empty($sanitize_cookie_final)) $sanitize_cookie_final=0;
        $where .= " AND ( $wpdb->posts.ID in (".$sanitize_cookie_final.') )';
        $where2=$this->query_checker($where,$checkkeyword);

        $ignored_products=get_option('guaven_woos_excluded_prods');
        if (!empty($ignored_products)) {
          $where2.=" and ($wpdb->posts.ID not in (".esc_sql($ignored_products)."))";
        }
    }
    return $where2;
}


    public function guaven_woos_tracker_inserter($failsuccess, $state, $froback, $unid)
    {
        $failed_arr=explode(", ", $failsuccess);
        $failed_arr_f[count($failed_arr)-1]=$failed_arr[count($failed_arr)-1];
        for ($i=count($failed_arr)-2;$i>=0;$i--) {
            if (strpos($failed_arr[$i+1], $failed_arr[$i])===false) {
                $failed_arr_f[$i]=$failed_arr[$i];
            }
        }
        $failed_arr_f=array_unique($failed_arr_f);
    //  var_dump($failed_arr_f);
    global $wpdb;
        foreach ($failed_arr_f as $faf) {
            if (!empty($faf)) {
                $wpdb->insert(
  $wpdb->prefix."woos_search_analytics",
  array(
    'keyword' => $faf,
    'created_date' => date("Y:m:d"),
    'user_info' => $unid,
    'state' => $state,
    'device_type'=>(wp_is_mobile()?'mobile':'desktop'),
    'side' => $froback
  ),
  array('%s','%s','%s','%s','%s'));
            }
        }
    }

    public function guaven_woos_tracker_callback()
    {
        if (!isset($_POST["failed"]) or !isset($_POST["success"]) or !isset($_POST["corrected"]) or !isset($_POST["unid"])) {
            exit;
        }
        global $wpdb;
        $current_timestamp=time();
        $addcontrol=esc_attr($_POST["addcontrol"]);
        if ($current_timestamp-intval($addcontrol)>3600) {
            exit;
        }
        check_ajax_referer('guaven_woos_tracker_'.$addcontrol, 'ajnonce');
        $this->guaven_woos_tracker_inserter($_POST["failed"], 'fail', 'frontend', $_POST["unid"]);
        $this->guaven_woos_tracker_inserter($_POST["success"], 'success', 'frontend', $_POST["unid"]);
        $this->guaven_woos_tracker_inserter($_POST["corrected"], 'corrected', 'frontend', $_POST["unid"]);
        exit;
    }
}
