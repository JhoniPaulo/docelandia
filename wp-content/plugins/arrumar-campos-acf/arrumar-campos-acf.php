<?php 

/**
 * Plugin Name: Corrigir campos importados do acf
 * Description: 
 *
*/

class corrigir_campos_acf_kwm {
 
    /**
     * Autoload method
     * @return void
     */
    public function __construct() {
        add_action( 'admin_menu', array(&$this, 'register_corrigir_campos_acf_kwm') );
    }
 
    /**
     * Register submenu
     * @return void
     */
    public function register_corrigir_campos_acf_kwm() {
        add_submenu_page( 
            'options-general.php', 'Corrigir campos importação', 'Corrigir campos importação', 'manage_options', 'corrigir_campos_acf_kwm', array(&$this, 'submenu_page_callback')
        );
    }
 
    /**
     * Render submenu
     * @return void
     */
    public function submenu_page_callback() {
        echo '<div class="wrap">';
        echo '<h2>Corrigir campos importação</h2>';
        echo '</div>';

        if( isset($_GET['atualizar']) && $_GET['atualizar'] == 'update_fields' ){
        	$products = get_posts(array(
				'post_type' => 'product',
				'posts_per_page' => - 1,
				'meta_query' => array(
					array(
						'key'     => '_adicionar_quantidade_minima_de_compras_woocommerce', //procure por um dos campos do acf que não importou corretamente
						'value'   => 'field_59a96106ff171', //procure por um dos campos do acf que não importou corretamente
						'compare' => 'NOT EXISTS',
					),
				),
			));

			$totalUpdates = 0;
        	if ($products)
			{
				foreach($products as $key => $value)
				{

					//aqui você pode alterar ou copiar da linha 57 ao 61 e alterar os valores dos campos do acf
					if (!add_post_meta($value->ID, '_adicionar_quantidade_minima_de_compras_woocommerce', 'field_59a96106ff171', true))
					{
					update_post_meta($value->ID, '_adicionar_quantidade_minima_de_compras_woocommerce', 'field_59a96106ff171');
					}

					if (!add_post_meta($value->ID, '_adicionar_tipo_de_quantidade_minima_woocommerce', 'field_5a8a2fd253f34', true))
					{
					update_post_meta($value->ID, '_adicionar_tipo_de_quantidade_minima_woocommerce', 'field_5a8a2fd253f34');
					}
					$totalUpdates++;
				}

			    echo '<div class="notice notice-success is-dismissible">';
			        echo '<p>Total de produtos atualizados: ' . $totalUpdates . '.</p>';
			    echo '</div>';
			}else{

			    echo '<div class="notice notice-success is-dismissible">';
			        echo '<p>Nenhum produto para atualizar.</p>';
			    echo '</div>';
			}

			sleep(2);
        }

        $products = get_posts(array(
			'post_type' => 'product',
			'posts_per_page' => - 1,
			'meta_query' => array(
				array(
					'key'     => '_adicionar_quantidade_minima_de_compras_woocommerce',//procure por um dos campos do acf que não importou corretamente
					'value'   => 'field_59a96106ff171',//procure por um dos campos do acf que não importou corretamente
					'compare' => 'NOT EXISTS',
				),
			),
		));
        $count = count($products);

        if ($products)
		{
			echo '<p>Você tem <strong>' . $count  . '</strong> produtos para atualizar.</p>';
			echo '<form method="get"><input type="hidden" value="corrigir_campos_acf_kwm" name="page"><input type="hidden" value="update_fields" name="atualizar"><button  class="button button-primary">Atualizar produtos</button></from>';
		}else{
			echo '<p>Todos os produtos estão atualizados.</p>';
		}

    }
 
}
 
new corrigir_campos_acf_kwm();