<?php

ob_start();
session_start();

/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$tdl_options = eva_global_var();

	//woocommerce_before_single_product_summary
	remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
	remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );
	
	add_action( 'woocommerce_before_single_product_summary_sale_flash', 'woocommerce_show_product_sale_flash', 10 );
	add_action( 'woocommerce_before_single_product_summary_product_images', 'woocommerce_show_product_images', 20 );
	
	//woocommerce_single_product_summary
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );
	
	add_action( 'woocommerce_single_product_summary_single_title', 'woocommerce_template_single_title', 5 );
	add_action( 'woocommerce_single_product_summary_single_rating', 'woocommerce_template_single_rating', 10 );
	add_action( 'woocommerce_single_product_summary_single_price', 'woocommerce_template_single_price', 10 );
	add_action( 'woocommerce_single_product_summary_single_excerpt', 'woocommerce_template_single_excerpt', 20 );
	add_action( 'woocommerce_single_product_summary_single_add_to_cart', 'woocommerce_template_single_add_to_cart', 30 );
	add_action( 'woocommerce_single_product_summary_single_meta', 'woocommerce_template_single_meta', 40 );
	add_action( 'woocommerce_single_product_summary_single_sharing', 'woocommerce_template_single_sharing', 50 );
	
	//woocommerce_after_single_product_summary
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
	
	add_action( 'woocommerce_after_single_product_summary_data_tabs', 'woocommerce_output_product_data_tabs', 10 );

	//custom actions
	add_action( 'woocommerce_before_main_content_breadcrumb', 'woocommerce_breadcrumb', 20, 0 );
	add_action( 'woocommerce_product_summary_thumbnails', 'woocommerce_show_product_thumbnails', 20 );

	$product_design = (!empty($tdl_options['tdl_product_design'])) ? $tdl_options['tdl_product_design'] : 'default'; 
	if (isset($_GET["product_design"])) $product_design = $_GET["product_design"];
?>

<style type="text/css">
	
	.woocommerce-variation-add-to-cart.variations_button.woocommerce-variation-add-to-cart-enabled, form.cart{
	    border: 1px solid #00286a;
	    background-color: rgba(0,40,106,0.15);
	    padding: 4%;
	}

	.quantity {
	    background-color: #fff;
	}	

	#button_keyup{
		position: relative;
	}

	#button_keyup span{
		position: absolute;
		left: 0;
		top: 40px;
		color: #00286a;
		font-weight: 700;
		font-size: 10pt;		
	}

	@media(max-width: 1100px){
		#button_keyup span{
			display: none;
		}
	}

	.woocommerce-product-details__short-description{
		display: none;
	}

</style>

<div class="page-header"></div>

<div id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

<div class="product_wrapper <?php echo esc_attr($product_design); ?>">
<div class="row">
	<div class="large-12 xlarge-10 xxlarge-9 large-centered columns"> 

	<?php
		/**
		 * woocommerce_before_single_product hook.
		 *
		 * @hooked wc_print_notices - 10
		 */
		 do_action( 'woocommerce_before_single_product' );

		 if ( post_password_required() ) {
		 	echo get_the_password_form();
		 	return;
		 }
	?>

		<div class="product_content_wrapper">

		<?php do_action( 'woocommerce_before_single_product' ); ?>

			<div class="row">

			<?php if (esc_attr($product_design) == "default") : ?>
				<div class="large-1 columns product_summary_thumbnails_wrapper">
					<div><?php do_action( 'woocommerce_product_summary_thumbnails' ); ?>&nbsp;</div>
				</div><!-- .columns -->	

				<div id="single-image" class="large-5 columns">				
			<?php else: ?>
				<div id="single-image" class="large-6 columns">				
			<?php endif; ?>

					<div class="product-images-wrapper">

						<?php				
							if ( (isset($tdl_options['tdl_catalog_mode'])) && ($tdl_options['tdl_catalog_mode'] == 0) ) {
								wc_get_template( 'loop/sale-flash.php' );
							}
							do_action( 'woocommerce_before_single_product_summary_product_images' );
							do_action( 'woocommerce_before_single_product_summary' );
						?>
					</div>
				</div><!-- .columns -->	
				


				<?php if (esc_attr($product_design) == "default") : ?>
				<div class="large-6 xxlarge-5 columns">
					<div class="product_infos">					
				<?php else: ?>
				<div class="large-6 xxlarge-5 columns" data-sticky-container>
					<div class="product_infos" data-sticky data-sticky-on="large" data-anchor="single-image" data-check-every="50" data-margin-top="7">		


				<?php endif; ?>

					<div class="product-inner-data">
							<div class="top_bar_shop_single">
								<?php eva_back_button() ?>
								<?php eva_products_nav(); ?>
								<div class="clearfix"></div>
							</div>


							 <div class="product_summary_top">

        						
								<?php
									do_action( 'woocommerce_single_product_summary_single_rating' );		
									do_action( 'woocommerce_single_product_summary_single_title' );
									
									if ( post_password_required() ) {
										echo get_the_password_form();
										return;
									}
								?>


							</div><!--.product_summary_top-->

<?php

	global $product;
	$_product = wc_get_product( get_the_ID());
	$preco = $_product->get_price();
	if(get_field('adicionar_tipo_de_quantidade_minima_woocommerce', get_the_ID()) == "cento"){
		$cento = $preco*100;
	}
	if($product->is_type( 'variable' )){	
		// $variations = $product->get_available_variations();
		// foreach ($variations as $key => $v) {
		// 	foreach ($v['attributes'] as $k => $r) {
		// 		//echo $r['attribute_pa_quantidade_minima'];
		// 		$aux = $aux.'/'.$r;
		// 	}
		// }
		// $aux2 = $aux; $r = explode('/', $aux2);	
	}

	?>

		<div id="info_qtd" qtd="<?php echo get_field('adicionar_quantidade_minima_de_compras_woocommerce', get_the_ID()); ?>" tipo="<?php echo get_field('adicionar_tipo_de_quantidade_minima_woocommerce', get_the_ID()); ?>">
			<div class="preco unidade">
				<div class="large-6">
					<p class="minimo">ENCOMENDA MÍNIMA: <span><?php echo get_field('adicionar_quantidade_minima_de_compras_woocommerce', get_the_ID()); ?> UNIDADE(S)</span></p>
				</div>
				<div class="large-5">
					<p class="unidade">PREÇO POR UNIDADE: <span>R$ 
						<?php echo number_format($preco, 2, '.', ',');?>
						</span></p>
				</div>								
			</div>

			<div class="preco kilo">
				<p class="cento">
					PREÇO POR QUILO: <span>R$ <?php echo number_format($preco, 2, '.', ',');?></span>
				</p>
				<p id="minimo">ENCOMENDA MÍNIMA: <span><?php echo get_field('adicionar_quantidade_minima_de_compras_woocommerce', get_the_ID()); ?> QUILO(S)</span></p>
			</div>
			<div class="preco cento">
				<div class="large-6">
					<p class="cento">PREÇO POR CENTO: <span>R$ 
						<?php echo number_format($cento, 2, '.', ',');?></span></p>
					<p id="minimo">ENCOMENDA MÍNIMA: <span><?php echo get_field('adicionar_quantidade_minima_de_compras_woocommerce', get_the_ID()); ?></span></p>
				</div>
				<div class="large-5">
					<p class="cento">PREÇO POR UNIDADE: <span>R$ <?php echo number_format($preco, 2, '.', ',');?></span></p>	
				</div>
				<div id="encomenda_minima" style="visibility: hidden;"><?php echo get_field('adicionar_quantidade_minima_de_compras_woocommerce', get_the_ID()); ?></div>
			</div>												
		</div>


						<?php
							do_action( 'woocommerce_single_product_summary_single_price' );
							do_action( 'woocommerce_single_product_summary_single_excerpt' );
							if ( (isset($tdl_options['tdl_catalog_mode'])) && ($tdl_options['tdl_catalog_mode'] == 0) ) {
								do_action( 'woocommerce_single_product_summary_single_add_to_cart' );
							}
						?>	

						<div class="product-buttons">
							<?php
								//eva_share();eva_size_chart();do_action( 'woocommerce_single_product_summary' );
							?>								
						</div>
					</div>


					
					</div>

				</div><!-- .columns -->
			</div><!-- .row -->
		</div><!-- .product_content_wrapper -->

	</div><!-- .columns -->
</div><!-- .row -->
</div>

<div class="row">
	<div class="large-12 large-centered columns description-section">
		<?php do_action( 'woocommerce_single_product_summary_single_meta' ); ?>
		<?php do_action( 'woocommerce_after_single_product_summary_data_tabs' ); ?>
	</div><!-- .columns -->
</div><!-- .row -->

<div class="row">
	<div class="large-9 large-centered columns">
		<?php
			do_action( 'woocommerce_single_product_summary_single_sharing' );
			do_action( 'woocommerce_after_single_product_summary' );
		?>     
	</div><!-- .columns -->
</div><!-- .row -->

<meta itemprop="url" content="<?php the_permalink(); ?>" />

</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>