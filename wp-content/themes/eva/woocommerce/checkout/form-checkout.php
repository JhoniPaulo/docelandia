<script type="text/javascript">
	jQuery(document).ready(function($){
		jQuery(".woocommerce-billing-fields h3").text("SEUS DADOS");
	});	
</script>
<style type="text/css">
	.alert-checkout, .payment_box p {
	    color: red;
	    font-size: 13pt;
	    font-weight: bold;
	}

	p#billing_country_field, p#billing_company_field, p#billing_address_1_field, p#billing_address_2_field, p#billing_city_field, p#billing_state_field, p#billing_postcode_field{
		display: none;
	}
	.cart-subtotal{
		display: none!important;
	}
</style>
<?php
/**
 * Checkout Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

?>

<?php
do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
	return;
}

// filter hook for include new pages inside the payment method
$get_checkout_url = apply_filters( 'woocommerce_get_checkout_url', WC()->cart->get_checkout_url() ); ?>
 	<p class="alert-checkout" style="text-align: center;">Você tem abaixo, um resumo dos ítens que acabou de escolher no site. Nesta área você pode preencher seus dados e nos enviar esse orçamento.  
IMPORTANTE: VOCÊ AINDA  NÃO ESTARÁ FECHANDO SUA COMPRA ao enviar o orçamento. Antes disso vamos ligar para você e confirmar certinho esse pedido.
Você também pode fazer a encomenda ligando nos telefones informados no site. </p>   
<div class="row">
    <div class="xxlarge-9 xlarge-10 large-12 large-centered columns">	
	
        <form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( $get_checkout_url ); ?>">
        
            <?php if ( sizeof( $checkout->checkout_fields ) > 0 ) : ?>
        
                <?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>
				<div class="row">
					
					<div class="large-7 columns">
						<div class="checkout_left_wrapper">
			
							<div class="col2-set" id="customer_details">
					
								<div class="col-1">
					
									<?php do_action( 'woocommerce_checkout_billing' ); ?>
						
								</div>
					
								<div class="col-2">
					
									<?php do_action( 'woocommerce_checkout_shipping' ); ?>
									<p class="alert-checkout">*taxa de entrega varia de acordo com o endereço. Vamos te ligar e esclarecer tudo. </p>
								</div>
					
							</div>
				
							<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>
							
						</div><!--.checkout_left_wrapper-->
					</div><!--.large-7-->					
					<div class="large-5 columns">
						<div class="checkout_right_wrapper custom_border">
							<div class="order_review_wrapper">
								
								<h2 id="order_review_heading"><?php echo "Seu Orçamento"; ?></h2>
								
								<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

								<div id="order_review" class="woocommerce-checkout-review-order">
									<?php do_action( 'woocommerce_checkout_order_review' ); ?>
								</div>

								<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
								
								<p style="color: red;
								font-size: 15pt;
								font-weight: bold;padding-top: 20px;">*Orçamentos tem validade de 15 dias.</p>
							</div><!--.order_review_wrapper-->
						</div><!--.checkout_right_wrapper-->
					</div><!--.large-5-->					
				</div><!--.row-->
					
            <?php endif; ?>
            
        </form>
   
    </div><!-- .columns -->
</div><!-- .row -->

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>