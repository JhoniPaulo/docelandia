<?php
/**
 * Single variation cart button
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
?>
<div class="woocommerce-variation-add-to-cart variations_button">
	<?php
		/**
		 * @since 3.0.0.
		 */
?>
<span id="button_keyup">
<?php
		do_action( 'woocommerce_before_add_to_cart_quantity' );

		woocommerce_quantity_input( array(
			'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : 1,
		) );
		?>
		<span></span>
</span>
<?php
		/**
		 * @since 3.0.0.
		 */
		do_action( 'woocommerce_after_add_to_cart_quantity' );
	?>
	<button type="submit" id="botao_orcamento" class="single_add_to_cart_button button btn1 bshadow alt"><span><?php echo esc_html( "Adicionar ao orçamento" ); ?><i class="button-loader"></i></span></button>

	<input type="hidden" name="add-to-cart" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="product_id" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="variation_id" class="variation_id" value="0" />
	<a href='<?php 


			global $woocommerce;
echo $checkout_url = $woocommerce->cart->get_cart_url();
?>' style="margin-left: 2%;"> > CONFERIR MEU ORÇAMENTO</a>
</div>

<?php if(has_excerpt()){ ?>
	<h5 style='margin-top: 4% !important;margin-top: 2%;'>DESCRIÇÃO DO PRODUTO</h5>
<?php the_excerpt();
} ?>
