<!DOCTYPE html>

<!--[if IE 9]>
<html class="ie ie9" <?php language_attributes(); ?>>
<![endif]-->

<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php esc_html(bloginfo( 'charset' )); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-4066214-2"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());
	 
	  gtag('config', 'UA-4066214-2');
	</script>

	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '880412092102980');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=880412092102980&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->


    
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php esc_html(bloginfo( 'pingback_url' )); ?>">


	<?php
		if (is_singular() && get_option('thread_comments'))
			wp_enqueue_script('comment-reply');

		wp_head();
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_url'); ?>/_assets/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_url'); ?>/_assets/css/animate.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_url'); ?>/_assets/css/hover.css">

	<script src="<?php echo get_bloginfo('template_url'); ?>/_assets/js/app.min.js"></script>
	<script src="<?php echo get_bloginfo('template_url'); ?>/_assets/js/jquery.cookie.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<?php if(is_product()){ ?>
<!-- 	<script type="text/javascript">
		jQuery(document).ready(function($){
			var n = $.cookie('redirect_sp');
			if(n == "nao"){
				$.cookie('redirect_sp', 'sim');
			}else{
				jQuery("#info_qtd").html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>');
				$.cookie('redirect_sp', 'nao');
				window.location.reload(true);			
			}	
			// alert(n);
		});
	</script> -->
<?php
	//setcookie("product_type", get_field('adicionar_tipo_de_quantidade_minima_woocommerce', get_the_ID()));
?>
<?php } ?>

	<style type="text/css">
		.eva_product_quick_view_button{
			display: none !important;
		}

		a.button.btn1.checkout.wc-forward.bshadow, input.button.btn2.update_cart_button, aside.widget.woocommerce.widget_product_tag_cloud {
		    display: none;
		}

		.page-header{
			margin-bottom: 0 !important;
		}

		header{
			background-color: #ebeffa !important;
		}	

		header.site-header .header-wrapper .nav .header-contact .header-contact-desc span{
			font-size: 12px !important;
			margin-bottom: 2px !important;
		}	

		header.site-header .header-wrapper .nav .header-contact .header-contact-desc h3{
			font-size: 19px !important;
			margin-top: 2px !important;
		}

		header.site-header .header-wrapper .tools ul li.cart-button .cart-desc span{
			font-size: 14px !important;
		}

		.home ul.products li {
		    padding: 0.4%;
		}

		.hotlink_home {
		    margin-top: -4%;
		}	

		ul.row.visible.products.products-grid.product-category-list.perspective_hover.small-up-2.medium-up-3.large-up-4.xlarge-up-4.xxlarge-up-4.animated.fadeIn {
		    margin: 0 auto !important;
		}

		.description-section{
			display: none;
		}

		.single_product_summary_related {
		    margin-top: 3%;
		}		

		footer#site-footer .f-copyright .copytxt p {
		    margin: 0;
		    padding: 0 0 1.5% 0;
		}

		p.product-category-listing a.product-category-link {
		    font-size: 8pt;
		}	
		
		#single-image .owl-item.active {
		    width: 90% !important;
		}		

		#contato_social img:hover{
			opacity: .7 !important;
			transition: all ease .4s !important;
		}

		ul.offcanvas_menu.mm-listview li.bot-menu-item.account-button,ul.offcanvas_menu.mm-listview li.bot-menu-item.logout-button{
			display: none !important;
		}

		footer#site-footer, ul.social-icons{
			background-color: #ebeffa !important;
		}

		.owl-stage-outer.owl-height{
			max-height: 780px !important;
		}

		a#orcamento_flutuante {
		    position: fixed;
		    bottom: 10.5%;
		    left: 0;
		    z-index: 9999;
		    padding: 20px;
		}		

		a#orcamento_flutuante i{
			color: #fff;
			font-style: normal;
			font-size: 17px;
		}
		a#orcamento_flutuante i:before {
		    font-family: "icomoon";
		    content: "\e900";
		}
		a#orcamento_flutuante:hover i{
			color: #00286a;
		}

		a#orcamento_flutuante span{
			max-width: 0px;
			transition: all 300ms;
			overflow: hidden;
			padding: 0;
			display: inline-block;
			font-size: 14px;
		}
		a#orcamento_flutuante span strong{
			display: block;
			width: 223px;
		}

		a#orcamento_flutuante:hover span{
			max-width: 400px;
		}

		div#produtos_relacionados span.price{display: block !important; float: left;}

		span#prod_r {
		    float: left;
		    margin-top: -7px;
		}		
		table.shop_table .product-quantity{
			display: none;
		}
		.woocommerce-cart .entry-content .woocommerce form table tbody td.product-name .variation,
		.woocommerce-cart .entry-content .woocommerce form table tbody td.product-name .variation dd,
		.woocommerce-cart .entry-content .woocommerce form table tbody td.product-name .variation dt,
		.woocommerce-cart .entry-content .woocommerce form table tbody td.product-name .variation dd p,
		.woocommerce-cart .entry-content .woocommerce form table tbody td.product-name .variation dt p{
			font-size: 14px;
		}
	</style>

	<meta name="description" content=" A Confeitaria Docelândia atua em Curitiba desde 1971 com o melhor da confeitaria. Tradição e
	Qualidade. Os mais deliciosos Doces, Salgados, Tortas, Bolos, Tortas, Sobremesas." />

	<meta name="author" content="Você Pop (www.vocepop.com.br) e Jhonatan Paulo (jhonatan.eti.br)">



</head>

<?php 
	$tdl_options = eva_global_var();
	$form_style = (!empty($tdl_options['tdl_form_style'])) ? $tdl_options['tdl_form_style'] : 'default'; 
	$color_scheme = (!empty($tdl_options['tdl_main_color_scheme'])) ? $tdl_options['tdl_main_color_scheme'] : 'mc_light';
	$header_layout = (!empty($tdl_options['tdl_header_layout'])) ? $tdl_options['tdl_header_layout'] : '1';
	if ( (isset($tdl_options['tdl_topbar_switch'])) && ($tdl_options['tdl_topbar_switch'] == "1") ) {
	  $topbar = 'has_topbar';
	} else {
	  $topbar = 'no_topbar';
	}	
 ?>

<body <?php body_class(); ?> data-form-style="<?php echo esc_attr($form_style); ?>" data-color-scheme="<?php echo esc_attr($color_scheme); ?>" data-topbar="<?php echo esc_attr( $topbar ); ?>" data-header-layout="<?php echo esc_attr( $header_layout ); ?>">
<?php
	global $post;
	$slug = get_post( $post )->post_name;
?>
<div style="position: absolute; top: -1000; left: -1000; visibility: hidden;" id="is_product"><?php echo is_product(); ?></div>

	<?php if ( (isset($tdl_options['tdl_page_loader'])) && (trim($tdl_options['tdl_page_loader']) == "1" ) ) : ?>
		<div id="eva-loader-wrapper">
			<div class="eva-loader-section">
				<div class="eva-loader-<?php echo esc_attr($tdl_options['tdl_page_loader_spinner']); ?>"></div>
			</div>
		</div>
	<?php endif; ?>

	    <?php if ( (isset($tdl_options['tdl_topbar_switch'])) && ($tdl_options['tdl_topbar_switch'] == "1" ) ) : ?>        
	    	<?php get_template_part( 'header', 'topbar' ); ?>                					
	    <?php endif; ?>


        <?php if ( isset($tdl_options['tdl_header_layout']) ) : ?>
								
			<?php if ( $tdl_options['tdl_header_layout'] == "1" ) : ?>
				<?php get_template_part( 'header', 'default' ); ?>
	        <?php elseif ( $tdl_options['tdl_header_layout'] == "2" ) : ?>
	        	<?php get_template_part( 'header', 'centered' ); ?>
			<?php else : ?>
				<?php get_template_part( 'header', 'left' ); ?>
			<?php endif; ?>		                                
	    <?php else : ?>                          
	            <?php get_template_part( 'header', 'default' ); ?>
        <?php endif; ?>	

       <?php 

	$woocommerce_cart_page_id_kwm = get_option( 'woocommerce_cart_page_id' ); 
	$woocommerce_checkout_page_id_kwm = get_option( 'woocommerce_checkout_page_id' ); 

	if(is_page($woocommerce_cart_page_id_kwm) || is_page($woocommerce_checkout_page_id_kwm)){
	}else{
		?>
	    <a href="<?php echo get_bloginfo('url');?>/meu-orcamento/" class="single_add_to_cart_button button btn1 bshadow alt" id="orcamento_flutuante">
	    	<i class="cart-button-icon"></i><span><strong>Conferir meu Orçamento<i class="button-loader"></i></strong></span>
	    </a>
		<?php
	}


	//com isso vamos gerar um style e esconder a sala de promoções da home
	$args = array(
	    'post_type'      => 'product',
	    'posts_per_page' => 8,
	    'meta_query'     => array(
	        'relation' => 'OR',
	        array( // Simple products type
	            'key'           => '_sale_price',
	            'value'         => 0,
	            'compare'       => '>',
	            'type'          => 'numeric'
	        ),
	        array( // Variable products type
	            'key'           => '_min_variation_sale_price',
	            'value'         => 0,
	            'compare'       => '>',
	            'type'          => 'numeric'
	        )
	    )
	);

	$saleProducts = new WP_Query( $args );

	if($saleProducts->post_count < 1){
		?>
		<style type="text/css">
			#promocoesId{
				display: none!important;
			}
		</style>
		<?php
	}


	?>
	<div class="offcanvas_container">
		<div class="offcanvas_main_content">
			<div class="page-wrapper">
